package com.samser.generatorproject;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.kafka.clients.producer.*;

import java.util.ArrayList;
import java.util.Random;
import java.util.Properties;
import java.util.Locale;

/**
 *
 * @author dok
 */
public class Main {
    
    final static Random random = new Random();
    static ArrayList<metricId> table;
    
    /**
     * В этом методе запускается продьюсер, после чего в него 
     * начинают отправляться сгенерированные сообщения
     * @param consumerName - имя, которое будет у Кафка-продьюсера
     */
     
    static ArrayList<metricId> table;
    
    private static void threadSleep(int mileSeconds)
    {
        try {
            Thread.sleep(mileSeconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    
     /**
     * Этот метод будет брать из Monitoring Controller таблицу с идентиыикаторами
     * метрик... когда-нибудь (пока что используется сгенерированная таблица)
     * @param host 
     * @param port
     * @return Таблица в виде ArrayList
     */
    public static ArrayList<metricId> getTable(String host, int port)
    {
      ArrayList<metricId> table = new ArrayList();
      
      URL url;
      HttpURLConnection connection = null;
              try{
            url = new URL("http://"+host+":"+port+"/getMetricsTable");
            connection = (HttpURLConnection) url.openConnection(); //Пытаемся открыть соединение
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Language", "en-US");
            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);
            
            //Отправляем запрос
            connection.connect();
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer();
                while((line = rd.readLine()) != null) {
                    response.append(line);
                    response.append('\r');
                }
                rd.close();

            //Получаем код ответа
            int code = connection.getResponseCode();
            
            //HTTP response code check
            if(code==200 && line.length()>0)
            {
                //Парсить массив в ответе
                JsonParser parser = new JsonParser();
                JsonObject mainObject = parser.parse(response.toString()).getAsJsonObject();
                JsonArray pItem = mainObject.getAsJsonArray("metrics");
                //Заполняем массив результатов, отталкиваясь от массива настроек
                for (JsonElement user : pItem) {
                    metricId temp = new Gson().fromJson(user, metricId.class);
                    table.add(temp);
                }
                return table;
            }
            else 
            {
                return null;
            }

        }
        catch(Exception e){
            System.out.println("Не удалось соединиться с сервером");
            //e.printStackTrace();
            System.out.println(e.getMessage());
            assert connection != null;
            connection.disconnect();
            return null;
        }
    }
    
     /**
     * В этом методе запускается продьюсер, после чего в него 
     * начинают отправляться сгенерированные сообщения
     * @param consumerName - имя, которое будет у Кафка-продьюсера
     */
    public static void produceStuff(String consumerName)
    {
      Properties props = new Properties();
      props.put("bootstrap.servers", "localhost:9092");
      props.put("acks", "all");
      props.put("retries", 0);
      props.put("batch.size", 16384);
      props.put("linger.ms", 1);
      props.put("buffer.memory", 33554432);
      props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
      props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

      Producer<String, String> producer1 = new KafkaProducer<>(props);
      //Producer<String, String> producer2 = new KafkaProducer<>(props);
      //Их больше одного будет
      
      while(true)
      {
        try{      
            String finalObj="";
            if(random.nextInt(2)==1)
            {
              finalObj = generateMetr(random.nextInt(table.size()));
            }
            else
            {
              finalObj = generateAlarm(random.nextInt(table.size()));
            }
            System.out.println(finalObj);
            producer1.send(new ProducerRecord<String, String>(consumerName, "lol",finalObj));            

            threadSleep(2000);  
            catch (InterruptedException e) {
              System.out.println("Error in sleep");
              e.printStackTrace();
            }   
        }catch(Exception e)
        {
            System.out.println("Error in producer");
            e.printStackTrace();
        }
      }
    }
    
     /**
     * Входные параметры программы: имя Кафка-продьюсера, ip и порт мониторинг-контроллера
     * в который будут отправляться объекты
     * @param args 
     */
    public static void main(String[] args)
    {
       if(args.length!=3)
       {
         System.out.println("usage: consumer name, monitoring controller host and port");
         System.exit(-1);
       }
       
       table = new ArrayList();
       
       //Заполнение таблицы данными из MC
       table = getTable(args[1],Integer.parseInt(args[2]));

       produceStuff(args[0]); 
    } 
    
    /**
     * Выбрать и вернуть случайную строку из предложенного массива
     * @param options - массив с вариантами строк
     * @return 
     */
    public static String generateString(ArrayList<String> options)
    {
       int idx = new Random().nextInt(options.size());
       return options.get(idx);
    }
    
    /**
     * Сгенерировать и вернуть ip-адрес
     * @return 
     */
    public static String generateIP()
    {
       return "196.168."+random.nextInt(255)+"."+random.nextInt(255);
    }
    
    /**
     * Сгенерировать число с плавающей точкой между min и max
     * @param min
     * @param max
     * @return 
     */
    public static double generateFloat(double min, double max)
    {
       return (random.nextFloat() * (max - min) + min);
    }
    
    /**
     * Сгенерировать и вернуть объект-метрику
     * @return 
     */
    public static String generateMetr(int option)
    {
       String obj = "{\"metric\":[{\"id\": \""+table.get(option).metricId+"\", \"timestamp\": "+
               System.currentTimeMillis()
               +", \"value\": " +String.format(Locale.US, "%.2f", generateFloat(1.0, 100.0))+"}]}";
       return obj;
    }
    
    /**
     * Сгенерировать и вернуть объект-алярм
     * @return 
     */
    public static String generateAlarm(int option)
    {
       ArrayList<String> options = new ArrayList();
       options.add("server is down"); options.add("server is up"); options.add("unknown server");
        String obj = "{\"alarm\":[{\"id\": \""+table.get(3*option).metricId+"\", \"timestamp\": "+
               System.currentTimeMillis()
               +", \"zabb_ver\": "+String.format(Locale.US, "%.1f", generateFloat(2.5, 3.4))
               +", \"msg\": \""+generateString(options)+"\", \"msg_type\": "+random.nextInt(10)+"}]}";
        
       return obj;
    }
}