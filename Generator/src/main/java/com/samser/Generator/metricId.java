package com.samser.generatorproject;

/**
 * Класс элементов таблиы метрик из Monitoring Controller
 * @author dok
 */
public class metricId {
    
    String metricId;
    String host;
    int port;
    String metricType;   
    
    metricId(String id, String host, int port, String mT)
    {
      this.metricId=id;
      this.host=host;
      this.port=port;
      this.metricType=mT;
    }
    
}