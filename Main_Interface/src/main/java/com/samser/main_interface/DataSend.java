package com.samser.main_interface;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Класс предполагался быть для другого, так что назван как назван
 * @author dok
 */
public class DataSend {

    HttpURLConnection connection;
    String ip;
    int http_port;
    int tcp_port;
    
    DataSend(String rcvd_ip,int h_port, int t_port)
    {
        this.ip=rcvd_ip;
        this.http_port=h_port;
        this.tcp_port=t_port;
    }

    public boolean isConnection()
    {
        return connection != null;
    }

    public boolean getRequest()
    {
        if (connection != null) {
            try {
                int code = connection.getResponseCode();
                return code == 200;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    /**
     * Кидаю на некий IP и порт POST http-запрос, в заголовке - свой ip и порт
     * Получаю ответ; Если он ОК, то продолжаем
     * @return true, если статус ответа на  запрос = 200
     */
    public boolean setConnection()
    {
        URL url;
        
        try{
            url = new URL("http://"+ip+":"+http_port+"/values/");
            connection = (HttpURLConnection) url.openConnection(); //Пытаемся открыть соединение
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Language", "en-US");
            connection.setRequestProperty("Port", ""+port);//custom header-port
            //Это внешний ip-шних вируалки с системой, на которой всё тестировалось
            connection.setRequestProperty("Address", "10.0.2.15");//custom header-ip
            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);
            
            //Отправляем запрос
            connection.connect();

            /*/Получаем код ответа
            int code = connection.getResponseCode();
            
            //HTTP response code check
            return code == 200;*/
            return true;
        }
        catch(Exception e){
            System.out.println("Не удалось соединиться с сервером");
            //e.printStackTrace();
            System.out.println(e.getMessage());
            assert connection != null;
            connection.disconnect();
            return false;
        }
    }
    
}
