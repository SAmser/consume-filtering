package com.samser.main_interface;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.ArrayList;

import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.common.serialization.StringDeserializer;

/**
 *
 * @author dok
 */
public class Main {

    private Properties getPropertiesKafkaConsumer() {
        Properties properties = new Properties();
        properties.put("bootstrap.servers", "localhost:9092");
        properties.put("group.id", "filter");
        properties.put("key.deserializer", StringDeserializer.class.getName());
        properties.put("value.deserializer", StringDeserializer.class.getName());
        return properties;
    }

    private void threadSleep(int mileSeconds)
    {
        try {
            Thread.sleep(mileSeconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void whileMethodReadSend(final KafkaConsumer<String, String> consumer, final String address, final int http_port,int tcp_port) {
        ArrayList<String> listMessages = new ArrayList<>();
        boolean[] workThread = new boolean[] { false, false, false };
        Thread threadCreateListMsg = new Thread(new Runnable() {
            private String getMsg30String() {
                int streamMsgSize = 0;
                String streamMsg = "";
                //¬ход: {УtsФ : <stamp>, УvalueФ: <string>}@{...}@...}
                while (streamMsgSize < 31) {
                    //ЅерЄм из  афки всЄ, что пришлЄт фильтр
                    ConsumerRecords<String, String> records = consumer.poll(Long.MAX_VALUE);
                    for (ConsumerRecord<String, String> record : records) {
                        Map<String, Object> data = new HashMap<>();
                        data.put("partition", record.partition());
                        data.put("offset", record.offset());
                        data.put("value", record.value());
                        //“ут скапливаетс¤ 30 штук данных
                        streamMsg += record.value()+"@";
                    }
                    streamMsgSize = streamMsgSize + 1;
                }
                streamMsg += "$";
                System.out.println(streamMsg);
                System.out.println();
                return streamMsg;
            }

            @Override
            public void run() {
                while (true) {
                    //Собсно, кидаем данные
                    String streamMsg = getMsg30String();
                    listMessages.add(streamMsg);
                    //System.out.println(in.readUTF()); // Если вдруг понадобится ответ
                }
            }
        });
        Thread threadSendMsgToServer = new Thread(new Runnable() {
            
            SocketStruct socketStruct = new SocketStruct(tcp_port);
            private void sendMsgToServerStorage() {
                workThread[2] = true;
                Socket socket = socketStruct.getSocket();
                System.out.println("Socket accepted");
                try {
                    if (listMessages.size() > 0) {
                        DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
                        String msg = listMessages.get(0);
                        listMessages.remove(0);
                        dataOutputStream.writeUTF(msg);
                        dataOutputStream.flush();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void run() {
                while (true) {
                    if (workThread[2] == false && listMessages.size() > 0)
                        sendMsgToServerStorage();
                    threadSleep(1000);
                }
            }
        });
        Thread threadSendHttpToServer = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true)
                {
                    if (workThread[2] == true && listMessages.size() > 0)
                    {
                           DataSend dataSend = new DataSend(address, http_port,tcp_port);
                           if (dataSend.setConnection() && dataSend.isConnection() && dataSend.getRequest()) {
                        }
                        workThread[2] = false;
                    }
                    threadSleep(300);
                }
            }
        });
        threadCreateListMsg.setName("Thread while create msg list");
        threadCreateListMsg.start();
        threadSendMsgToServer.setName("Thread send msg to server");
        threadSendMsgToServer.start();
        threadSendHttpToServer.setName("Thread send http request");
        threadSendHttpToServer.start();
    }

    public static void whileMethod(String topic, String address,int h_port,int t_port)
    {
        Properties properties = getPropertiesKafkaConsumer();
        ArrayList<String> topics = new ArrayList<>();
        topics.add(topic);
        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(properties);
        consumer.subscribe(topics);
        whileMethodReadSend(consumer, address, h_port,t_port);
    }

    /**
     * @param args Входные параметры программы: адрес и порт сервиса хранения, имя Кафка-консьюмера, из которого берутся данные
     */
    public void main(String[] args) {

        if (args.length != 4) {
            System.out.println("usage: storage ip, storage http port, storage tcp port, consumer name");
            System.exit(1);
        }

        whileMethod(args[3], args[0], Integer.parseInt(args[1]),Integer.parseInt(args[2]));
    }

}