package pkg3minterface;

import java.net.Socket;
import java.net.ServerSocket;
import java.net.SocketException;
import java.io.*;

/**
 *
 * @author alexey2093
 */
        public class SocketStruct
    {
        private ServerSocket serverSocket;

        public SocketStruct(int port)
        {
            try {
                this.serverSocket = new ServerSocket(port);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public boolean isSocketRun() {
            return serverSocket != null;
        }

        public Socket getSocket() {
            try {
                return serverSocket.accept();
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
