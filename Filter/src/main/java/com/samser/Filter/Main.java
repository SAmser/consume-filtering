package com.samser.filterproject;

import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.common.errors.WakeupException;
import org.apache.kafka.common.serialization.StringDeserializer;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonElement;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

/**
 *
 * @author dok
 */
public class Main {
    
    /**
     * Метод, который берёт из Кафки алармы и метрики и отправляет их на фильтрацию
     * @param topics - список топиков-источников данных
     * @param config - массив с настройками
     * @param target - имя топика, в который будут помещаться очищенные данные
     */
    private static void listenStuff(ArrayList<String> topics, boolean[][] config, String target)
    {
      Properties props;
      KafkaConsumer<String, String> consumer;
             
      props = new Properties();
      props.put("bootstrap.servers", "localhost:9092");
      props.put("group.id", "filter");
      props.put("key.deserializer", StringDeserializer.class.getName());
      props.put("value.deserializer", StringDeserializer.class.getName());

      consumer = new KafkaConsumer<String, String>(props);
      Parse_n_Send pns = new Parse_n_Send(config,target);
      
      try 
      {
      consumer.subscribe(topics);

      while (true) {
          ConsumerRecords<String, String> records = consumer.poll(Long.MAX_VALUE);
          for (ConsumerRecord<String, String> record : records) {
            Map<String, Object> data = new HashMap<>();
            data.put("partition", record.partition());
            data.put("offset", record.offset());
            data.put("value", record.value());
            //System.out.println(data);
            pns.filterSome(record.value());
          }
        }
        } catch (WakeupException e) {
          // ignore for shutdown 
        } finally {
          consumer.close();
        }
      
    }
    
    /**
     * Попробовать открыть конфигурационный файл и взять оттуда текст
     * Если всё хорошо - вернуть текст
     * @param path - путь к файлу
     * @return 
     */
    public static String readFile(String path)
    {
      String res = null;
      try
      {
        BufferedReader reader = new BufferedReader( new FileReader (path));
        String line = null;
        StringBuilder stringBuilder = new StringBuilder();
        String ls = System.getProperty("line.separator");
        while( ( line = reader.readLine() ) != null ) 
        {
          stringBuilder.append( line );
          stringBuilder.append( ls );
         }
         stringBuilder.deleteCharAt(stringBuilder.length()-1);
         res = stringBuilder.toString();
      }catch(Exception e)
      {
        e.printStackTrace();
        System.out.println("Error: cant read file");
        System.exit(-1);  
      }
      
      return res;
    }
    
    /**
     * Проверить поле на пустоту и вернуть значение, если всё хорошо
     * @param conf - json-элемент с конфигурацией
     * @param fname - имя поля в json-объекте
     * @return 
     */
    public static int checkfield(JsonElement conf,String fname)
    {
        int field=0;
        try{
            JsonObject userObject = conf.getAsJsonObject();
            field=userObject.get(fname).getAsInt();
        }catch(NullPointerException npe)
        {
            field=0;
        }
        return field;
    }
    
    /**
     * Получить из конфигурационного файла параметры
     * @param content - содержимоей файла
     * @return - массив с настройками
     */
    public static boolean[][] parseConfig(String content)
    {
        boolean[][] config=new boolean[2][3];
        try
        {
          //parsing
          JsonParser parser = new JsonParser();
          JsonObject mainObject = parser.parse(content).getAsJsonObject();
          JsonArray pItem; 
          pItem = mainObject.getAsJsonArray("Config");
          JsonElement sett = pItem.get(0).getAsJsonObject();
          String[] fields1 = {"value"};
          boolean thereisdata = false;
          for(int i = 0; i<fields1.length;i++)
          if(checkfield(sett,fields1[i])==1)
          {
            config[0][i]=true; 
            thereisdata=true;
          }
          
          if(!thereisdata)
          {
            System.out.println("Error: there is no fields to show in config");
            System.exit(-1); 
          }
          
          String[] fields2 = {"zver","msg","msg_type"};
          thereisdata = false;
          sett = pItem.get(1).getAsJsonObject();
          for(int i = 0; i<fields2.length;i++)
          if(checkfield(sett,fields2[i])==1)
          {
            config[1][i]=true; 
            thereisdata=true;
          }
          
          if(!thereisdata)
          {
            System.out.println("Error: there is no fields to show in config");
            System.exit(-1); 
          }
          
        }
        catch(Exception e)
        {
           e.printStackTrace();
           System.out.println("Error: wrong config in file");
           System.exit(-1); 
        }
        return config;
    }
    
    /**
     * Входные параметры программы: Кафка-топик, откуда брать данные,
     * Путь к конфигурационному файлу, топик, куда отправлять очищенные данные
     * @param args 
     */
    public static void main(String[] args)
    {
       String file_cont=null;
       boolean[][] config = null;
       
       if(args.length!=3)
       {
         System.out.println("usage: source topic, config path, target topic");
         System.exit(-1);
       }
       File f = new File(args[1]);
       if(!f.exists())
       {
         System.out.println("Error: no such file");
         System.exit(-1); 
       }
       else
       {
            file_cont = readFile(args[1]);
            config = parseConfig(file_cont);      
       }  
       if(config == null)
       {
         System.out.println("Error: bad config");
         System.exit(-1);  
       } 
       
       ArrayList<String> topics = new ArrayList();
       topics.add(args[0]);
       
       listenStuff(topics,config,args[2]);
    } 
}
