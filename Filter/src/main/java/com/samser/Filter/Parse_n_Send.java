package com.samser.filterproject;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.Properties;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

/**
 *
 * @author dok
 */
public class Parse_n_Send {
    
    boolean[][] config;
    Properties props;
    String consumName;
    Producer<String, String> producer;
    
    
    Parse_n_Send(boolean[][] sent_conf,String name)
    {
        consumName = name;
        this.config = sent_conf;
        props = new Properties();
        props.put("bootstrap.servers", "localhost:9092");
        props.put("acks", "all");
        props.put("retries", 0);
        props.put("batch.size", 16384);
        props.put("linger.ms", 1);
        props.put("buffer.memory", 33554432);
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer"); 
        producer = new KafkaProducer<>(props);
    }
    
    /**
     * 3. Наконец, отфильтрованный объект кладётся в финальный топик
     * @param res 
     */
    private void putInTopic(String res)
    {
       try{
       producer.send(new ProducerRecord<String, String>(consumName, "lol",res));
       }catch(Exception e)
        {
            System.out.println("Error: Message to consumer "+consumName+"haven't send: "+e.getMessage());
        }
    }
    
    /**
     * 2a. Метод, фильтрующий алямр, отталкиваясь от конфигурации
     * @param al 
     */
    private void filterAlarm(JsonArray al)
    {
        Alarm[] a = new Gson().fromJson(al, Alarm[].class);
        int cnt = 0;
        String res="{\"mid\": "+a[0].id+", \"alarm\":{";
        
        for(int i = 0; i< config[1].length;i++)
        {
          if(config[1][i])  
          {
              if(cnt>0){
              res+=", ";
              }
              switch(i){
                  case 0: {res+="\"zabbix ver\": "+a[0].zabb_ver; break;}
                  case 1: {res+="\"text\": \""+a[0].msg+"\""; break;}
                  case 2: {res+="\"msg_type\": "+a[0].msg_type; break;}
              }
              cnt++;
          } 
        }
        res+="}}";
        res=res.replaceAll("\"","\\\\\"");
        
        res += "\"}";
        res = "{\"ts\": "+a[0].timestamp+", \"value\": \"" + res;
        putInTopic(res);
        //System.out.println(res);
    }
    
    /**
     * 2b. Метод, фильтрующий метрику, отталкиваясь от конфигурации
     * @param mtr - собственно, объект-метрика в формате JSON
     */
    private void filterMetric(JsonArray mtr)
    {
        Metric[] m = new Gson().fromJson(mtr, Metric[].class);
        int cnt = 0;
        String res="{\"mid\": "+m[0].id+", \"metric\":{";
        
        for(int i = 0; i< config[0].length;i++)
        {
          if(config[0][i])  
          {
              if(cnt>0){
                res+=", ";
              }
              switch(i){
                case 0: {res+="\"value\": "+m[0].value; break;}
              }
              cnt++;
          } 
        }
        res+="}}";
        res=res.replaceAll("\"","\\\\\"");
        res += "\"}";
        
        res = "{\"ts\": "+m[0].timestamp+", \"value\": \"" + res;
        
        putInTopic(res);
        //System.out.println(res);
    }


    /**
     * 1. В этот метод помещается json объект-алярм или -метрика и, в зависимости от
     * того, какого вида объект, он помещается в нужный метод парсинга
     * и фильтрации
     * @param obj - собственно,объект
     */
    public void filterSome(String obj)
    {

      JsonParser parser = new JsonParser();
      JsonObject mainObject = parser.parse(obj).getAsJsonObject();
      JsonArray pItem; 
      pItem = mainObject.getAsJsonArray("metric");
      if(pItem==null)
      {
        if(mainObject.getAsJsonArray("alarm")!=null)
        {
          pItem = mainObject.getAsJsonArray("alarm");
          //Parse as alarm
          filterAlarm(pItem); 
        }
      }
      else
      {
        //Parse as metric
        filterMetric(pItem);
      }   
        
    }
    
}
