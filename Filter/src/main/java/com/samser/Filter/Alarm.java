/**
 * Класс объекта-алярма
 * @author dok
 * {"ts": <tstamp>, "value": "{\"metric_id\": <id>, \"alarm\":{\"msg_text\": \"unknown server\"}}"}
 */
public class Alarm {
    String id;
    long timestamp;
    String zabb_ver;
    String msg;
    byte msg_type;  
}