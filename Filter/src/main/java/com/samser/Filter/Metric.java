package com.samser.filterproject;

/**
 * Класс объекта-метрики
 * @author dok
 * {"ts": <tstamp>, "value": "{\"metric_id\": <id>, \"metric\":{\"value\": <float от 1.0 до 100.0 >}}"}
 */
public class Metric {
    String id;
    long timestamp;
    float value;
}
